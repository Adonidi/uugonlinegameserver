﻿public enum CommandsClientToServer : ushort
{
  NONE = 0,
  HANDSHAKE = 1,
  MESSAGE = 2,
  REQUEST_ALL_BATTLES,
  NEW_BATTLE,
  JOIN_BATTLE,
  CLICK_IN_BATTLE,
  LEAVE_BATTLE
}
