﻿using GameData;


namespace OnlineGameServer
{
  public class ServerPerson : Person
  {
    public UserOnServer user = null;

    public ServerBattle battle { get; private set; }
    public BattleData.TeamIdx team_idx { get; private set; }


    public ServerPerson( ulong person_id, string person_name )
      : base( person_id, person_name )
    {
    }

    public void disconnect()
    {
      user = null;
      battle?.removePerson( this, team_idx );
      joinBattle( null, BattleData.TeamIdx.NONE );
    }

    public void joinBattle( ServerBattle battle, BattleData.TeamIdx team_idx )
    {
      this.battle = battle;
      this.team_idx = team_idx;

      if ( battle == null || user == null)
        return;

      if ( battle.addPerson( this, team_idx ) )
        user.sendBattleJoined( battle, team_idx );
      else
      {
        user.sendBattleJoinFailed();
        joinBattle( null, BattleData.TeamIdx.NONE );
      }
    }

    public void leaveBattle()
    {
      if ( battle == null || user == null )
        return;

      battle.removePerson( this, team_idx );
    }

    public void battleClick()
    {
      battle?.addPoints( 1, team_idx );
    }
  }
}
