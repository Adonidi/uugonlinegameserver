﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameData;


namespace OnlineGameServer
{
  public class ServerBattle : BattleData
  {
    private bool battle_ongoing = true;
    private readonly object battle_lock_object = new object();


    public ServerBattle( int battle_id )
      : base( battle_id )
    {
    }

    private void forEachServerPerson( Action<ServerPerson> act )
    {
      lock( battle_lock_object )
        foreach ( ServerPerson sp in left_team.Concat( right_team ) )
          act( sp );
    }

    public new bool addPerson( Person person, TeamIdx team_idx )
    {
      if ( !battle_ongoing )
        return false;

      lock ( battle_lock_object )
        if ( base.addPerson( person, team_idx ) )
        {
          sendPersonAdded( person, team_idx );
          return true;
        }

      return false;
    }

    public new void removePerson( Person person, TeamIdx team_idx )
    {
      lock ( battle_lock_object )
      {
        base.removePerson( person, team_idx );
        sendPersonRemoved( person, team_idx );

        if ( left_team.Count == 0 && right_team.Count == 0 )
        {
          battle_ongoing = false;
          BattlesHolder.inst.removeBattle( this );
        }
      }
    }

    public new void addPoints( int points, TeamIdx team_idx )
    {
      if ( !battle_ongoing )
        return;

      lock ( battle_lock_object )
      {
        base.addPoints( points, team_idx );
        sendUpdatedPoints();
        checkForWin();
      }
    }

    private void checkForWin()
    {
      lock ( battle_lock_object )
      {
        if ( winner == TeamIdx.NONE )
          return;

        battle_ongoing = false;
        sendBattleFinished( winner );
      }
    }

    #region Senders

    private void sendPersonAdded( Person person, TeamIdx team_idx )
    {
      MyDbg.writeLine( "ServerBattle.sendPersonAdded()" );

      forEachServerPerson( sp => sp.user.sendPersonAdded( person, team_idx ) );
    }

    private void sendPersonRemoved( Person person, TeamIdx team_idx )
    {
      MyDbg.writeLine( "ServerBattle.sendPersonRemoved()" );

      forEachServerPerson( sp => sp.user.sendPersonRemoved( person, team_idx ) );
    }

    private void sendUpdatedPoints()
    {
      MyDbg.writeLine( "ServerBattle.sendUpdatedPoints()" );

      forEachServerPerson( sp => sp.user.sendUpdatedPoints( leftTeamScore, rightTeamScore ) );
    }

    private void sendBattleFinished( TeamIdx team_idx )
    {
      MyDbg.writeLine( "ServerBattle.sendBattleFinished()" );

      forEachServerPerson( sp => sp.user.sendBattleFinished( team_idx ) );
    }
    #endregion
  }
}
