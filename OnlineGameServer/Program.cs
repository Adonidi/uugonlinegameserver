﻿using System;
using System.Threading;
using ConnectionLib;


namespace OnlineGameServer
{
  internal class Program
  {
    private const int PORT = 11000;

    public static bool exitting = false;


    private static void Main( string[] args )
    {
      MyDbg.writeLine = Console.WriteLine;

      IAsynchronousListener listener = ConnectorLibFactory.createAsynchronousListener();

      UsersHolder.inst.init( listener );

      listener.startListening( PORT );

      while ( true )
      {
        if ( Console.ReadKey().Key == ConsoleKey.Escape )
          break;
      }

      listener.stopListening();
      exitting = true;
      Thread.Sleep( 100 );
    }
  }
}
