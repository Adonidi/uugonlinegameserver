﻿using System;
using System.IO;
using GameData;


namespace OnlineGameServer
{
  public partial class UserOnServer
  {
    public void sendMessage( string msg )
    {
      MyDbg.writeLine( $"UserOnServer.sendMessage()   msg {msg}" );

      connector.send( (ushort)CommandsServerToClient.MESSAGE, stream =>
      {
        BinaryWriter bw = new BinaryWriter( stream );
        bw.Write( msg );
      } );
    }

    private void sendHandshaked()
    {
      MyDbg.writeLine( "UserOnServer.sendHandshaked()" );

      connector.send( (ushort)CommandsServerToClient.HANDSHAKED, stream =>
      {
        BinaryWriter bw = new BinaryWriter( stream );
        person.toBytes( bw );
      } );
    }

    public void sendPersonAdded( Person person, BattleData.TeamIdx team_idx )
    {
      connector.send( (ushort)CommandsServerToClient.PERSON_ADDED, stream =>
      {
        BinaryWriter bw = new BinaryWriter( stream );
        person.toBytes( bw );
        bw.Write( (byte)team_idx );
      } );
    }

    public void sendPersonRemoved( Person person, BattleData.TeamIdx team_idx )
    {
      connector.send( (ushort)CommandsServerToClient.PERSON_REMOVED, stream =>
      {
        BinaryWriter bw = new BinaryWriter( stream );
        person.toBytes( bw );
        bw.Write( (byte)team_idx );
      } );
    }

    public void sendUpdatedPoints( int left_team_score, int right_team_score )
    {
      connector.send( (ushort)CommandsServerToClient.UPDATED_POINTS, stream =>
      {
        BinaryWriter bw = new BinaryWriter( stream );
        bw.Write( left_team_score );
        bw.Write( right_team_score );
      } );
    }

    public void sendBattleFinished( BattleData.TeamIdx team_idx )
    {
      connector.send( (ushort)CommandsServerToClient.BATTLE_FINISHED, stream =>
      {
        BinaryWriter bw = new BinaryWriter( stream );
        bw.Write( (byte)team_idx );
      } );
    }

    public void sendBattleJoined( ServerBattle battle, BattleData.TeamIdx team_idx )
    {
      connector.send( (ushort)CommandsServerToClient.BATTLE_JOINED, stream =>
      {
        BinaryWriter bw = new BinaryWriter( stream );
        battle.toBytes( bw );
        bw.Write( (byte)team_idx );
      } );
    }

    public void sendBattleJoinFailed()
    {
      connector.send( (ushort)CommandsServerToClient.BATTLE_JOIN_FAILED, stream => {} );
    }

    private void sendAllBattles( ServerBattle[] battles )
    {
      connector.send( (ushort)CommandsServerToClient.ALL_BATTLES, stream =>
      {
        BinaryWriter bw = new BinaryWriter( stream );
        bw.Write( battles.Length );
        foreach ( ServerBattle battle in battles )
          battle.toBytesSimple( bw );
      } );
    }
  }
}
