﻿using System;


namespace OnlineGameServer
{
  public partial class UserOnServer
  {
    public ulong user_id = 0;

    public event Action onHandshake = delegate { };

    private IInConnector connector = null;

    private ServerPerson person = null;

    public bool isDead => connector == null || connector.isDead;


    public UserOnServer( IInConnector connector )
    {
      updateConnector( connector );
    }

    internal void mergeInto( UserOnServer user )
    {
      connector.onCommand -= handleCommand;
      user.updateConnector( connector );
    }

    private void updateConnector( IInConnector connector )
    {
      if ( this.connector != null )
      {
        this.connector.onCommand -= handleCommand;
        this.connector.close();
      }

      this.connector = connector;
      connector.onCommand += handleCommand;
    }

    public void disconnect()
    {
      MyDbg.writeLine( $"UserOnServer.disconnect()   user_id {user_id}" );

      connector?.close();
      person.disconnect();
    }
  }
}
