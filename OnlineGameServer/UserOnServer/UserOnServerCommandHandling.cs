﻿using System;
using System.IO;
using GameData;


namespace OnlineGameServer
{
  public partial class UserOnServer
  {
    private void parseMessage( BinaryReader command_data )
    {
      string msg = command_data.ReadString();

      MyDbg.writeLine( $"UserOnServer.parseMessage()   user_id {user_id}   msg {msg}" );

      sendMessage( "Answer for your msg!" );
    }

    private void parseHandshake( BinaryReader br )
    {
      user_id = br.ReadUInt64();
      MyDbg.writeLine( $"UserOnServer.parseHandshake()   user_id {user_id}" );

      person = PersonHolder.inst.getPerson( user_id );
      person.user = this;

      onHandshake();

      sendHandshaked();
    }

    private void parseAllBattlesRequest( BinaryReader br )
    {
      MyDbg.writeLine( $"UserOnServer.parseAllBattlesRequest()   user_id {user_id}" );

      sendAllBattles( BattlesHolder.inst.getCurrentBattles() );
    }

    private void parseNewBattle( BinaryReader br )
    {
      MyDbg.writeLine( $"UserOnServer.parseNewBattle()   user_id {user_id}" );

      ServerBattle sb = BattlesHolder.inst.newBattle();
      person.joinBattle( sb, BattleData.TeamIdx.LEFT );
    }

    private void parseJoinBattle( BinaryReader br )
    {
      MyDbg.writeLine( $"UserOnServer.parseJoinBattle()   user_id {user_id}" );

      int battle_id = br.ReadInt32();
      BattleData.TeamIdx team_idx = (BattleData.TeamIdx)br.ReadByte();

      ServerBattle battle = BattlesHolder.inst.getBattle( battle_id );
      if ( battle == null )
        sendBattleJoinFailed();

      person.joinBattle( battle, team_idx );
    }

    private void parseClickInBattle( BinaryReader br )
    {
      MyDbg.writeLine( $"UserOnServer.parseClickInBattle()   user_id {user_id}" );
      person.battleClick();
    }

    private void parseLeaveBattle( BinaryReader br )
    {
      MyDbg.writeLine( $"UserOnServer.parseClickInBattle()   user_id {user_id}" );
      person.leaveBattle();
    }

    private void handleCommand( ushort command_num, Stream command_data )
    {
      CommandsClientToServer command = (CommandsClientToServer)command_num;
      BinaryReader br = new BinaryReader( command_data );

      MyDbg.writeLine( $"UserOnServer.handleCommand()   command_num {Enum.GetName( typeof( CommandsClientToServer ), command )}" );

      switch ( command )
      {
      case CommandsClientToServer.HANDSHAKE          : parseHandshake( br );         break;
      case CommandsClientToServer.MESSAGE            : parseMessage( br );           break;
      case CommandsClientToServer.REQUEST_ALL_BATTLES: parseAllBattlesRequest( br ); break;
      case CommandsClientToServer.NEW_BATTLE         : parseNewBattle( br );         break;
      case CommandsClientToServer.JOIN_BATTLE        : parseJoinBattle( br );        break;
      case CommandsClientToServer.CLICK_IN_BATTLE    : parseClickInBattle( br );     break;
      case CommandsClientToServer.LEAVE_BATTLE       : parseLeaveBattle( br );       break;

      default:
        MyDbg.writeLine( $"   Error: bad command!" );
        disconnect();
        break;
      }
    }
  }
}
