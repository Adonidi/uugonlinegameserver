using System.Collections.Generic;
using System.Linq;
using System.Threading;


namespace OnlineGameServer
{
  public class UsersHolder
  {
    private readonly Dictionary<ulong, UserOnServer> all_users = new Dictionary<ulong, UserOnServer>();
    private readonly object users_lock_object = new object();

    private IAsynchronousListener listener = null;

    private static UsersHolder _inst;
    public static UsersHolder inst => _inst ?? ( _inst = new UsersHolder() );


    private UsersHolder()
    {
      Thread thread = new Thread( cleanUp );
      thread.Start();
    }

    public void init( IAsynchronousListener listener )
    {
      if ( this.listener != null )
        this.listener.onNewClientAccepted -= onNewClient;

      this.listener = listener;
      listener.onNewClientAccepted += onNewClient;
    }

    private void onNewClient( IInConnector in_connector )
    {
      UserOnServer user_on_server = new UserOnServer( in_connector );
      user_on_server.onHandshake += () =>
      {
        lock ( users_lock_object )
        {
          UserOnServer user = null;
          all_users.TryGetValue( user_on_server.user_id, out user );
          if ( user == null )
          {
            all_users[user_on_server.user_id] = user_on_server;
          } else
          {
            user_on_server.mergeInto( user );
          }
        }
      };
    }

    public void cleanUp()
    {
      while ( !Program.exitting )
      {
        Thread.Sleep( 100 );
        lock ( users_lock_object )
        {
          UserOnServer[] dead_users = all_users.Values.Where( x => x.isDead ).ToArray();
          foreach ( UserOnServer user in dead_users )
          {
            user.disconnect();
            all_users.Remove( user.user_id );
          }
        }
      }
    }
  }
}
