﻿using System.Collections.Generic;
using System.Linq;
using GameData;


namespace OnlineGameServer
{
  public class BattlesHolder
  {
    private readonly Dictionary<int, ServerBattle> all_battles = new Dictionary<int, ServerBattle>();
    private readonly object battles_lock_object = new object();

    private static BattlesHolder _inst;
    public static BattlesHolder inst => _inst ?? ( _inst = new BattlesHolder() );


    private BattlesHolder() { }

    public ServerBattle getBattle( int battle_id )
    {
      MyDbg.writeLine( $"BattlesHolder.getBattle()   battle_id {battle_id}" );

      ServerBattle bd = null;

      lock ( battles_lock_object )
      {
        all_battles.TryGetValue( battle_id, out bd );

        if ( bd == null )
        {
          MyDbg.writeLine( "   not found, creating new." );

          bd = new ServerBattle( battle_id );
          all_battles[battle_id] = bd;
        }
      }

      return bd;
    }

    public ServerBattle newBattle()
    {
      int battle_id = 0;
      lock ( battles_lock_object )
        battle_id = all_battles.Count == 0 ? 1 : all_battles.Keys.Max() + 1;

      return getBattle( battle_id );
    }

    public void removeBattle( ServerBattle bd )
    {
      lock ( battles_lock_object )
        all_battles.Remove( bd.battle_id );
    }

    public ServerBattle[] getCurrentBattles()
    {
      lock ( battles_lock_object )
        return all_battles.Values.ToArray();
    }
  }
}
