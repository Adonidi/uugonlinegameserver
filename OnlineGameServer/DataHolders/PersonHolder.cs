﻿using System.Collections.Generic;
using GameData;


namespace OnlineGameServer
{
  public class PersonHolder
  {
    private readonly Dictionary<ulong, ServerPerson> all_persons = new Dictionary<ulong, ServerPerson>();
    private readonly object persons_lock_object = new object();

    private static PersonHolder _inst;
    public static PersonHolder inst => _inst ?? ( _inst = new PersonHolder() );


    private PersonHolder() { }

    public ServerPerson getPerson( ulong person_id )
    {
      MyDbg.writeLine( $"PersonHolder.getPerson()   person_id {person_id}" );

      ServerPerson p = null;

      lock ( persons_lock_object )
      {
        all_persons.TryGetValue( person_id, out p );

        if ( p == null )
        {
          MyDbg.writeLine( "   not found, creating new." );

          p = new ServerPerson( person_id, $"Person{person_id}" );
          all_persons[person_id] = p;
        }
      }

      return p;
    }
  }
}
