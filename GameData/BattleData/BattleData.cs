﻿using System;
using System.Collections.Generic;
using System.IO;
using GameData;


namespace GameData
{
  public class BattleData
  {
    public enum TeamIdx
    {
      NONE,
      LEFT,
      RIGHT
    }

    public const int SCORE_FOR_WIN = 10;


    public int battle_id { get; private set; }

    public int leftTeamScore { get; private set; }
    public int rightTeamScore { get; private set; }

    protected readonly List<Person> left_team  = new List<Person>();
    protected readonly List<Person> right_team = new List<Person>();


    public TeamIdx winner
    {
      get
      {
        if ( leftTeamScore >= SCORE_FOR_WIN )
          return TeamIdx.LEFT;
        if ( rightTeamScore >= SCORE_FOR_WIN )
          return TeamIdx.RIGHT;
        return TeamIdx.NONE;
      }
    }


    public BattleData()
    {
    }

    protected BattleData( int battle_id )
    {
      this.battle_id = battle_id;
    }

    protected bool addPerson( Person person, TeamIdx team_idx )
    {
      switch ( team_idx )
      {
      case TeamIdx.LEFT:
        left_team.Add( person );
        break;
      case TeamIdx.RIGHT:
        right_team.Add( person );
        break;
      default:
        return false;
      }

      return true;
    }

    protected void removePerson( Person person, TeamIdx team_idx )
    {
      switch ( team_idx )
      {
      case TeamIdx.NONE:
        return;
      case TeamIdx.LEFT:
        left_team.RemoveAll( x => person.personId == x.personId );
        break;
      case TeamIdx.RIGHT:
        right_team.RemoveAll( x => person.personId == x.personId );
        break;
      default:
        return;
      }
    }

    protected void addPoints( int points, TeamIdx team_idx )
    {
      switch ( team_idx )
      {
      case TeamIdx.LEFT:
        leftTeamScore += points;
        break;
      case TeamIdx.RIGHT:
        rightTeamScore += points;
        break;
      default:
        return;
      }
    }

    protected void updatePoints( int left_team_score, int right_team_score )
    {
      this.leftTeamScore = left_team_score;
      this.rightTeamScore = right_team_score;
    }

    public void fromBytes( BinaryReader br )
    {
      fromBytesSimple( br );

      int left_cnt = br.ReadInt32();
      int right_cnt = br.ReadInt32();

      left_team.Clear();
      for ( int i = 0; i < left_cnt; i++ )
        left_team.Add( new Person( br ) );

      right_team.Clear();
      for ( int i = 0; i < right_cnt; i++ )
        right_team.Add( new Person( br ) );
    }

    public void fromBytesSimple( BinaryReader br )
    {
      battle_id = br.ReadInt32();

      leftTeamScore = br.ReadInt32();
      rightTeamScore = br.ReadInt32();
    }

    public void toBytes( BinaryWriter bw )
    {
      toBytesSimple( bw );

      bw.Write( left_team.Count );
      bw.Write( right_team.Count );

      foreach ( Person person in left_team )
        person.toBytes( bw );

      foreach ( Person person in right_team )
        person.toBytes( bw );
    }

    public void toBytesSimple( BinaryWriter bw )
    {
      bw.Write( battle_id );

      bw.Write( leftTeamScore );
      bw.Write( rightTeamScore );
    }
  }
}
