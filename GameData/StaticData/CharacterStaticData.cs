﻿using System.IO;


namespace GameData
{
  public class CharacterStaticData
  {
    private int char_id;
    private int stat_max_health;
    private int stat_attack;

    public int charId { get { return char_id; } private set { char_id = value; } }
    public int statMaxHealth { get { return stat_max_health; } private set { stat_max_health = value; } }
    public int statAttack { get { return stat_attack; } private set { stat_attack = value; } }


    public CharacterStaticData()
    {
    }

    internal CharacterStaticData( int char_id, int stat_max_health, int stat_attack )
    {
      this.charId        = char_id;
      this.statMaxHealth = stat_max_health;
      this.statAttack    = stat_attack;
    }

    public void toBytes( BinaryWriter bw )
    {
      bw.Write( charId );
      bw.Write( statMaxHealth );
      bw.Write( statAttack );
    }

    public void fromBytes( BinaryReader br )
    {
      charId        = br.ReadInt32();
      statMaxHealth = br.ReadInt32();
      statAttack    = br.ReadInt32();
    }
  }
}
