﻿using System.Collections.Generic;

namespace GameData
{
  public class StaticCharactersHolder
  {
    private readonly Dictionary<int, CharacterStaticData> all_characters = new Dictionary<int, CharacterStaticData>();
    public IEnumerable<CharacterStaticData> allCharacters => all_characters.Values;

    private static StaticCharactersHolder _inst;
    public static StaticCharactersHolder inst => _inst ?? ( _inst = new StaticCharactersHolder() );


    private StaticCharactersHolder()
    {
      all_characters[1] = new CharacterStaticData( 1, 100, 10 ) ;
      all_characters[2] = new CharacterStaticData( 2, 150, 6 );
      all_characters[3] = new CharacterStaticData( 3, 70, 13 );
    }

    public CharacterStaticData getCharacterInfo( int id )
    {
      return all_characters.ContainsKey( id ) ? all_characters[id] : null;
    }
  }
}
