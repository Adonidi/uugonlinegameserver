﻿using System.IO;


namespace GameData
{
  public class Person
  {
    private ulong person_id;
    private string person_name;

    public ulong personId { get { return person_id; } set { person_id = value; } }
    public string personName { get { return person_name; } set { person_name = value; } }


    public Person( ulong person_id, string person_name )
    {
      this.personId = person_id;
      this.personName = person_name;
    }

    public Person( BinaryReader br )
    {
      fromBytes( br );
    }

    public Person()
    {
    }

    public void fromBytes( BinaryReader br )
    {
      personId = br.ReadUInt64();
      personName = br.ReadString();
    }

    public void toBytes( BinaryWriter bw )
    {
      bw.Write( personId );
      bw.Write( personName );
    }
  }
}
