﻿using System;


public interface IAsynchronousListener
{
  event Action<IInConnector> onNewClientAccepted;

  void startListening( int port );
  void stopListening();
}
