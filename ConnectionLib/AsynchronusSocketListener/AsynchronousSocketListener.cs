﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;


internal class AsynchronousSocketListener : IAsynchronousListener
{
  private readonly ManualResetEvent all_done = new ManualResetEvent( false );
  private bool is_listening = false;

  public event Action<IInConnector> onNewClientAccepted = delegate { };


  public void startListening( int port )
  {
    if ( is_listening )
      return;

    Thread thread = new Thread( listenThread );
    thread.Start( port );
  }

  private void listenThread( object o )
  {
    int port = (int)o;

    IPHostEntry ipHostInfo   = Dns.GetHostEntry( Dns.GetHostName() );
    IPAddress ipAddress      = ipHostInfo.AddressList[0];
    IPEndPoint localEndPoint = new IPEndPoint( ipAddress, port );

    Socket listener = new Socket( ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp );
    is_listening = true;

    try
    {
      listener.Bind( localEndPoint );
      listener.Listen( 100 );

      while ( is_listening )
      {
        all_done.Reset();

        MyDbg.writeLine( "Waiting for a connection..." );
        listener.BeginAccept( acceptCallback, listener );

        all_done.WaitOne();
      }
    }
    catch ( Exception e )
    {
      is_listening = false;
      MyDbg.writeLine( e.ToString() );
    }

    MyDbg.writeLine( $"listen finished for port {port}" );
  }

  public void stopListening()
  {
    is_listening = false;
    all_done.Set();
  }

  private void acceptCallback( IAsyncResult ar )
  {
    all_done.Set();

    Socket listener = (Socket)ar.AsyncState;
    Socket handler = listener.EndAccept( ar );

    SocketWrapperIn client = new SocketWrapperIn( handler );

    MyDbg.writeLine( $"New client accepted: {handler.RemoteEndPoint}" );
    onNewClientAccepted( client );

    client.startWork();
  }
}
