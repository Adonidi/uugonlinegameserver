﻿using System.Net;


namespace ConnectionLib
{
  public static class ConnectorLibFactory
  {
    public static IOutConnector createOutConnection( IPAddress ip_address, int port )
    {
      return new SocketWrapperOut( ip_address, port );
    }

    public static IAsynchronousListener createAsynchronousListener()
    {
      return new AsynchronousSocketListener();
    }
  }
}
