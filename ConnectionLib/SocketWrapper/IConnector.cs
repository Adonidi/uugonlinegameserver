﻿using System;
using System.IO;


public interface IInConnector
{
  bool isDead { get; }

  event Action<ushort, Stream> onCommand;

  void startWork();
  void close();
  void send( ushort command_num, Action<Stream> write_command_content );
}

public interface IOutConnector : IInConnector
{
  event Action onConnected;
}
