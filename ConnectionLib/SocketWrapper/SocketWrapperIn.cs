using System;
using System.IO;
using System.Net.Sockets;


internal class SocketWrapperIn : IInConnector
{
  private const int BUFFER_SIZE = 1024;
  private const int HEADER_SIZE = 4;

  protected readonly Socket work_socket = null;
  private readonly byte[] buffer        = new byte[BUFFER_SIZE];
  private MemoryStream ms               = new MemoryStream();
  private long last_read_pos            = 0;
  private readonly object lock_obj      = new object();

  public event Action<ushort, Stream> onCommand = delegate { };


  public SocketWrapperIn( Socket handler )
  {
    work_socket = handler;
  }


  public bool isDead => work_socket.Poll( 1000, SelectMode.SelectRead ) && ( work_socket.Available == 0 );

  public virtual void startWork()
  {
    beginRecieve();
  }

  public void close()
  {
    work_socket.Shutdown( SocketShutdown.Both );
    work_socket.Close();
  }

  #region Send
  private static void writeHeader( ushort command_num, MemoryStream ms )
  {
    ushort len = (ushort)( ms.Length < HEADER_SIZE ? 0 : ms.Length - HEADER_SIZE );
    byte[] buf = BitConverter.GetBytes( command_num );
    ms.Write( buf, 0, buf.Length );
    buf = BitConverter.GetBytes( len );
    ms.Write( buf, 0, buf.Length );
  }

  private void sendCallback( IAsyncResult ar )
  {
    try
    {
      // Complete sending the data to the remote device.  
      int bytes_sent = work_socket.EndSend( ar );
      MyDbg.writeLine( $"Sent {bytes_sent} bytes." );
    }
    catch ( Exception e )
    {
      MyDbg.writeLine( "sendCallback() exception" );
    }
  }

  public void send( ushort command_num, Action<Stream> write_command_content )
  {
    if ( isDead )
      return;

    try
    {
      using ( MemoryStream ms = new MemoryStream() )
      {
        ms.Position = HEADER_SIZE;
        write_command_content( ms );
        ms.Position = 0L;

        writeHeader( command_num, ms );

        // Begin sending the data to the remote device.
        work_socket.BeginSend( ms.GetBuffer(), 0, (int)ms.Length, 0, sendCallback, this );
      }
    }
    catch ( Exception e )
    {
      MyDbg.writeLine( "send(ushort, Action<Stream>) exception" );
    }
  }
  #endregion

  #region Receive
  protected void beginRecieve()
  {
    if ( isDead )
      return;

    try
    {
      work_socket.BeginReceive( buffer, 0, BUFFER_SIZE, 0, readCallback, this );
    }
    catch ( Exception e )
    {
      MyDbg.writeLine( "beginReceive() exception" );
    }
  }

  private bool processCommand()
  {
    if ( isDead )
      return false;

    if ( ms.Length < HEADER_SIZE )
      return false;

    ms.Position = last_read_pos;

    BinaryReader br = new BinaryReader( ms );

    //Header
    ushort command_num = br.ReadUInt16();
    ushort command_len = br.ReadUInt16();

    if ( ms.Length < ms.Position + command_len )
      return false;

    onCommand( command_num, ms );

    if ( ms.Position != last_read_pos + HEADER_SIZE + command_len )
      throw new Exception( "processCommand() Bad protocol!" );

    if ( ms.Position == ms.Length )
    {
      ms.Dispose();
      ms = new MemoryStream();
    }

    last_read_pos = ms.Position;

    return true;
  }

  private void readCallback( IAsyncResult ar )
  {
    int bytes_read = 0;

    try
    {
      bytes_read = work_socket.EndReceive( ar );
    }
    catch ( Exception e )
    {
      MyDbg.writeLine( "readCallback() exception (connection closed)" );
      return;
    }

    if ( bytes_read < 0 )
      return;
    lock ( lock_obj )
    {
      ms.Position = ms.Length;
      ms.Write( buffer, 0, bytes_read );

      try
      {
        processCommand();
      }
      catch ( Exception )
      {
        MyDbg.writeLine( "processCommand() exception" );
      }
    }

    beginRecieve();
  }
  #endregion
}
