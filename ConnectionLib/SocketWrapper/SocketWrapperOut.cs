using System;
using System.Net;
using System.Net.Sockets;


internal class SocketWrapperOut : SocketWrapperIn, IOutConnector
{
  private readonly IPEndPoint remote_ep;

  public event Action onConnected = delegate { };


  public SocketWrapperOut( IPAddress ip_address, int port )
    : base( new Socket( ip_address.AddressFamily, SocketType.Stream, ProtocolType.Tcp ) )
  {
    remote_ep = new IPEndPoint( ip_address, port );
  }

  private void connectCallback( IAsyncResult ar )
  {
    try
    {
      work_socket.EndConnect( ar );

      MyDbg.writeLine( $"Socket connected to {work_socket.RemoteEndPoint}" );
      onConnected();
      beginRecieve();
    }
    catch ( Exception e )
    {
      MyDbg.writeLine( "connectCallback() exception" );
    }

  }

  public override void startWork()
  {
    work_socket.BeginConnect( remote_ep, connectCallback, this );
  }
}